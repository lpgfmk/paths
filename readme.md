# Readme

## Installation
Please refer to the [Laravel documentation](https://laravel.com/docs/6.0) for setting up the entire project (not really needed though).
You may simply copy the `app/Path.php` class wherever you like running php and play with `Path::show`. 

## Points of interest
`app/Path.php` - Implementation of `cd` function

`tests/Feature/PathTest.php` - Implementation of tests for this feature

If you actually install the project, opening the URL `/dev` will also show any failing tests, which were used as part 
of the dev process.
