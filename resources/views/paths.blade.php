<html>
<title>Paths</title>
<body>

<div class="flex-center position-ref full-height">
    <div class="content">
        <div>
            <h1>Current test</h1>
            <p>Test path is: {{ $dir }}</p>
            <p>Result is: {{ $path->currentPath }}</p>

        </div>
        @if(count($errors))
            <div>
                <h1>Current tests with errors</h1>

                @foreach($errors as $error)
                    <p>{{ $error }}</p>
                @endforeach
            </div>
        @else
            <div>
                <h1>All tests are green</h1>
            </div>
        @endif

    </div>
</div>
</body>
</html>
