<?php

namespace App\Http\Controllers;

use App\Path;
use Tests\Feature\PathTest;

class PathController extends Controller
{
    /**
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function index()
    {
        $dir = "/a/b/c/d";
        $path = new Path($dir);
        $path->setDelimiter("~");
        $path->setDelimiterRoot("#");
        $path->cd('~a~b~c');

        return view('paths', [
            'path' => $path,
            'dir' => $dir,
            'errors' => $this->getErrors()]);
    }

    /**
     * Wrapper for viewing
     *
     * @throws \Exception
     */
    public function show()
    {
        $path = new Path("/a/b/c/d");
        $path->show();
    }

    /**
     * Frontend calling of tests
     *
     * @return array
     */
    public function getErrors()
    {
        $errors = [];
        try {
            (new PathTest())->basic_canCdToDirOnCurrentLevel();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->basic_canCdToDirOnCurrentLevelDotPrefixed();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->basic_canCdToDirOnUpperLevelRelative();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->basic_canCdToDirOnRootLevel();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->basic_canCdToDirUpOneLevel();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdToDirWithSpacesInName();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdToDirWithDotInName();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdToDirWithTwiDotsInName();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdWhenPathIsEmpty();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdWhenPathIsNumber();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdWhenPathIsLongText();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdWhenPathIsTextAndNumbers();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdWhenDir1IsWrongButDir2IsOk();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdToPathOnlyContainingDelimiters();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdToPathOnlyContainingRootDelimiters();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdToPathOnlyContainingStrangeRootDelimiters();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdWhenRootIsSameWithDelimiter();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdWhenRootIsDifferentFromDelimiter();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        try {
            (new PathTest())->error_canCdToRootWhenRootIsDifferentFromDelimiter();
        } catch (\Exception $e) {
            $errors[] = $e->getTrace()[3]['function'];
        }
        return $errors;
    }
}
