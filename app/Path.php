<?php

namespace App;


class Path
{
    /*
     * Supported actions for cd
     */
    const GOTONOTSUPPORTED = 0;
    const GOTOFILE = 1;
    const GOTODIR = 2;
    const GOTODIRCURRENT = 3;
    const GOTODIRPARENT = 4;
    const GOTODIRROOT = 5;
    const GOTODIRROOTANDCARRY = 6;

    /*
     * Special characters
     */
    const PATH_DIR_PARENT = '..';
    const PATH_DIR_CURRENT = '.';

    public $currentPath;
    protected $backupPath;
    protected $delimiter = '/';
    protected $delimiterRoot = '/';

    /**
     * @param string $delimiter
     */
    public function setDelimiter(string $delimiter): void
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @param string $delimiterRoot
     * @throws \Exception
     */
    public function setDelimiterRoot(string $delimiterRoot): void
    {
        $delimiterUsedForTheRegexp = '~';
        if (strcmp($delimiterRoot, $delimiterUsedForTheRegexp) == 0) {
            throw new \Exception("You can't use this character for delimiter. Sorry.");
        }
        $this->delimiterRoot = $delimiterRoot;
    }

    private $notes = "
        ok - root path is '/'.
        ok - path separator is '/'.
        ok - parent directory is addressable as '..'.
        ok - the function will not be passed any invalid paths.
        ok - directory names consist only of English alphabet letters (A-Z and a-z).
        do not use built-in path-related functions.";

    /**
     * Path constructor.
     * @param $path
     */
    function __construct($path)
    {
        $this->currentPath = $path;
        $this->backupPath = $this->currentPath;
    }

    /**
     * Echo the currentPath
     *
     * @throws \Exception
     */
    public function show()
    {
        $dir = "/a/b/c/d";
        $path = new Path($dir);
//        $path->setDelimiter("~");
//        $path->setDelimiterRoot("#");
        $path->cd('/x/y/z');
        echo $path->currentPath;
    }

    /**
     * Change directory uses sensible rules for setting
     * a new path. Can receive absolute or relative
     * $new_paths.
     *
     * @param $new_path
     * @return mixed
     */
    public function cd($new_path)
    {
        $exploded = explode($this->delimiter, $new_path);
        $exploded = $this->removeTrailingDelimiter($exploded);
        $countOneDelimiterMatch = 0;

        foreach ($exploded as $change) {
            $action = $this->translateIntended($change);

            switch ($action) {
                case self::GOTODIR:
                    $next = $this->currentPath . $this->delimiter . $change;
                    $this->currentPath = $this->validatedPath($next);
                    break;
                case self::GOTODIRCURRENT:
                    $next = $this->currentPath;
                    $this->currentPath = $this->validatedPath($next);
                    break;
                case self::GOTODIRPARENT:
                    $goToDirParentPath = explode($this->delimiter, $this->currentPath);
                    array_pop($goToDirParentPath);
                    $this->currentPath = implode($this->delimiter, $goToDirParentPath);
                    break;
                case self::GOTODIRROOT:
                    if (!$countOneDelimiterMatch) {
                        $next = "";
                        $this->currentPath = $this->validatedPath($next);
                        $countOneDelimiterMatch++;
                    } else {
                        $next = $this->backupPath;
                        $this->currentPath = $this->validatedPath($next);
                    }
                    break;
                case self::GOTODIRROOTANDCARRY:
                    if (!$countOneDelimiterMatch) {
                        $next = substr($change, 1, strlen($change));
                        $this->currentPath = $this->validatedPath($this->delimiterRoot . $next);
                        $countOneDelimiterMatch++;
                    } else {
                        $next = $this->backupPath;
                        $this->currentPath = $this->validatedPath($next);
                    }
                    break;
                case self::GOTONOTSUPPORTED:
                default:
                    break 2;
            }
        }

        return $this->currentPath;
    }

    /**
     * Dictionary that matches intended use with available
     * actions to perform.
     *
     * @param $path - One level intended action: dir name,
     *              root path, up one level, ...
     * @return int  - One of the GOTO constants
     */
    private function translateIntended($path)
    {
        switch ($path) {
            /*
             * We reach this point when $this->delimiterRoot ==
             * $this->delimiter. When the $new_path is exploded
             * the delimiterRoot is matched, so replaced by ""
             */
            case "":
                $intended = self::GOTODIRROOT;
                break;
            /*
             * We reach this point when $this->delimiterRoot !=
             * $this->delimiter. When the $new_path is exploded
             * the delimiterRoot gets stuck to the root dir.
             */
            case (preg_match("~^{$this->delimiterRoot}[a-zA-Z]+$~", $path) ? true : false):
                $intended = self::GOTODIRROOTANDCARRY;
                break;
            case self::PATH_DIR_CURRENT:
                $intended = self::GOTODIRCURRENT; // current dir
                break;
            case self::PATH_DIR_PARENT:
                $intended = self::GOTODIRPARENT; // parent dir
                break;
            case (preg_match('/^[a-zA-Z]+$/', $path) ? true : false):
                $intended = self::GOTODIR; // dir name
                break;
            default:
                $intended = self::GOTONOTSUPPORTED; // not supported
                break;
        }
        return $intended;
    }

    /**
     * Placeholder for future path validation. This will
     * need to check the path points to a valid dir or
     * file in the system.
     *
     * @param $target
     * @return mixed
     */
    private function validatedPath($target)
    {
        return $target;
    }

    /**
     * Remove the trailing delimiter to ensure we
     * always respond with the same kind of path
     *
     * @param array $exploded
     * @return array
     */
    private function removeTrailingDelimiter(array $exploded): array
    {
        $lastElementOfArray = $exploded[count($exploded) - 1];

        /*
         * $lastElementOfArray will be "" after the self::DELIMITER
         * is exploded. This breaks self::GOTODIRROOT part so we
         * remove the element
         */
        if (strcmp($lastElementOfArray, "") == 0) {
            array_pop($exploded);
        }
        return $exploded;
    }
}

