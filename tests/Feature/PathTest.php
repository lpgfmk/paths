<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PathTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function basic_canCdToDirOnCurrentLevel()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('x');
        $this->assertEquals('/a/b/c/d/x', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function basic_canCdToDirOnCurrentLevelDotPrefixed()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('./x');
        $this->assertEquals('/a/b/c/d/x', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function basic_canCdToDirUpOneLevel()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('../x');
        $this->assertEquals('/a/b/c/x', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function basic_canCdToDirOnRootLevel()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('/a');
        $this->assertEquals('/a', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function basic_canCdToDirOnUpperLevelRelative()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('../../e/../f');
        $this->assertEquals('/a/b/f', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function basic_canCdToDirOnRootLevelRelative()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('/d/e/../a');
        $this->assertEquals('/d/a', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdToDirWithSpacesInName()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd(' ');
        $this->assertEquals('/a/b/c/d', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdToDirWithDotInName()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('.x');
        // requirement is that path names like .x aren't supported
        // when this changes just append .x below
        $this->assertEquals('/a/b/c/d', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdToDirWithTwiDotsInName()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('..x');
        $this->assertEquals('/a/b/c/d', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdWhenPathIsEmpty()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd(' ');
        $this->assertEquals('/a/b/c/d', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdWhenPathIsNumber()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('9321321321321321321321321321321321321321321321321');
        $this->assertEquals('/a/b/c/d', $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdWhenPathIsLongText()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss');
        $this->assertEquals('/a/b/c/d/sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss',
            $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdWhenPathIsTextAndNumbers()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('asdfasdfasdfasdfasd112312312312312312323');
        $this->assertEquals('/a/b/c/d',
            $path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdWhenDir1IsWrongButDir2IsOk()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('#asdf/zxcv');
        $this->assertEquals('/a/b/c/d',$path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdToPathOnlyContainingDelimiters()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->cd('/////');
        $this->assertEquals('/a/b/c/d',$path->currentPath);
    }
    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdToPathOnlyContainingRootDelimiters()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->setDelimiter("/");
        $path->setDelimiterRoot("#");
        $path->cd('####');
        $this->assertEquals('/a/b/c/d',$path->currentPath);
    }
    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdToPathOnlyContainingStrangeRootDelimiters()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->setDelimiterRoot("$");
        $path->cd('####');
        $this->assertEquals('/a/b/c/d',$path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdWhenRootIsSameWithDelimiter()
    {
        $path = new \App\Path('/a/b/c/d');
        $path->setDelimiter("/");
        $path->setDelimiterRoot("/");
        $path->cd('x/y/z');
        $this->assertEquals('/a/b/c/d/x/y/z',$path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdWhenRootIsDifferentFromDelimiter()
    {
        $path = new \App\Path('#a/b/c/d');
        $path->setDelimiter("/");
        $path->setDelimiterRoot("#");
        $path->cd('x/y/z');
        $this->assertEquals('#a/b/c/d/x/y/z',$path->currentPath);
    }

    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function error_canCdToRootWhenRootIsDifferentFromDelimiter()
    {
        $path = new \App\Path('#a/b/c/d');
        $path->setDelimiter("/");
        $path->setDelimiterRoot("#");
        $path->cd('#x/y/z');
        $this->assertEquals('#x/y/z',$path->currentPath);
    }

}
